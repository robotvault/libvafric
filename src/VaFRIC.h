#ifndef _vaFRIC_H_
#define _vaFRIC_H_

/* Copyright (c) 2013 Ankur Handa
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#undef isfinite
#undef isnan

#include<iostream>
#include<string>
#include<TooN/se3.h>

#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <TooN/TooN.h>
#include <TooN/se3.h>
#include <cstring>
#include <dirent.h>
#include <cvd/image.h>
#include <cvd/image_io.h>

#include <vector>
#include "boost/random.hpp"
#include "boost/generator_iterator.hpp"
#include <boost/random/normal_distribution.hpp>
#include <algorithm>
//#include <vector_types.h>
//#include <helper_math.h>

#include<cutil_math.h>


using namespace std;

namespace dataset{

typedef boost::mt19937 RNGType; ///< mersenne twister generator


template<class T>
double gen_normal_3(T &generator)
{
  return generator();
}

// Version that fills a vector
template<class T>
void gen_normal_3(T &generator,
              std::vector<double> &res)
{
  for(size_t i=0; i<res.size(); ++i)
    res[i]=generator();
}


class vaFRIC
{

public:

    vaFRIC(string _filebasename,
           int _img_width,
           int _img_height,
           float _u0,
           float _v0,
           float _focal_x,
           float _focal_y)
        :filebasename(_filebasename),
          txtfilecount(0),
          pngfilecount(0),
          depthfilecount(0),
          img_width(_img_width),
          img_height(_img_height),
          u0(_u0),
          v0(_v0),
          focal_x(_focal_x),
          focal_y(_focal_y)

    {
        int len;
        struct dirent *pDirent;
        DIR *pDir = NULL;

        pDir = opendir(filebasename.c_str());
        if (pDir != NULL)
        {
            while ((pDirent = readdir(pDir)) != NULL)
            {
                len = strlen (pDirent->d_name);
                if (len >= 4)
                {
                    if (strcmp (".txt", &(pDirent->d_name[len - 4])) == 0)
                    {
                        txtfilecount++;
                    }
                    else if (strcmp (".png", &(pDirent->d_name[len - 4])) == 0)
                    {
                        pngfilecount++;
                    }
                    else if (strcmp (".depth", &(pDirent->d_name[len - 6])) == 0)
                    {
                        depthfilecount++;
                    }
                }
            }
            closedir (pDir);
        }

        if ( txtfilecount != pngfilecount || txtfilecount != depthfilecount || pngfilecount != depthfilecount)
            std::cerr<< "Warning: The number of depth files, png files and txt files are not same."<<endl;


        std::cout<<"Going to read all the label images"<<std::endl;

        no_labels = 0;


        for(int l =0; l < 255; l++)
            colours[l] = -1;


        for(int img_no =0 ; img_no < pngfilecount; img_no++)
        {
            char* fileName  = new char[300];

            sprintf(fileName,"%s/scene_00_%04d.png",filebasename.c_str(),img_no);

//            std::cout<<"FileName = " << fileName << std::endl;

            CVD::Image< CVD::Rgb<CVD::byte> >labelImage;
            CVD::img_load(labelImage,fileName);

            for(int v = 0; v < img_height; v++)
            {
                for(int u = 0; u < img_width; u++)
                {
                    CVD::Rgb<CVD::byte>rgb_pixel = labelImage[CVD::ImageRef(u,v)];

                    int label = (int)(rgb_pixel.red + rgb_pixel.green*256 + rgb_pixel.blue*256*256);

                    int label_exists = false;

                    for(int i = 0 ; i < no_labels; i++)
                    {
                        if ( colours[i] == label )
                        {
                            label_exists = true;
                            break;
                        }
                    }
                    if (!no_labels || !label_exists)
                    {
                        colours[no_labels] = label;

                        no_labels++;
                    }
                }
            }
        }

        std::cout<<"Total number of labels = " << no_labels << std::endl;

        for(int l =0; l < 255; l++)
        {
//            std::cout<<"[l = " << colours[l]<<"] ";

            if ( colours[l] != -1 )
            {
                CVD::Rgb<CVD::byte>pixel_colour;

                int label_val       = colours[l];

                int label_val_hat   = (label_val)%(256*256);

                pixel_colour.blue   = (label_val - label_val_hat)/(256*256);
                pixel_colour.green  = (label_val_hat - label_val_hat%256)/256;
                pixel_colour.red    = (label_val_hat%256);

                std::cout<<"[l = " << l<<"] ";

                std::cout<<"red = " << (int)pixel_colour.red
                         <<", green =" << (int)pixel_colour.green
                         <<", blue = "<<(int)pixel_colour.blue<<std::endl;
            }
        }
//        getchar();


//        for(int img_no =0 ; img_no < pngfilecount; img_no++)
//        {
//            char* fileName  = new char[300];

//            sprintf(fileName,"%s/scene_00_%04d.png",filebasename.c_str(),img_no);

//            CVD::Image< CVD::Rgb<CVD::byte> >labelImage;
//            CVD::img_load(labelImage,fileName);

//            for(int v = 0; v < img_height; v++)
//            {
//                for(int u = 0; u < img_width; u++)
//                {
//                    CVD::Rgb<CVD::byte>rgb_pixel = labelImage[CVD::ImageRef(u,v)];

//                    if ( rgb_pixel.red == 255 && rgb_pixel.green == 255
//                         && rgb_pixel.green == 255 )
//                    {
//                        std::cout<<fileName << std::endl;
//                        getchar();
//                    }
//                }
//            }

//        }

//        std::cout<<"Looped through twice the png files" << std::endl;

//        getchar();

    }

    /// Obtain the pose of camera Tpov_cam, with respect to povray world
    TooN::SE3<> computeTpov_cam(int ref_img_no, int which_blur_sample);

    /// POVRay gives euclidean distance of a point from camera, so convert it to obtain depth
    /// If a depth_array pointer is needed, use float* depth = &depth_array[0];
    void getEuclidean2PlanarDepth(int ref_img_no, int which_blur_sample, std::vector<float> &depth_array);
    void getEuclidean2PlanarDepth(int ref_img_no, int which_blur_sample, float* depth_array);

    /// If there are semantic labels/annotations - read them into a label array.
    void getLabelsforImage(int ref_img_no, int which_blur_sample, std::vector<float>& label_array);
    void writeLabeltoImage(int ref_img_no, int which_blur_sample, std::vector<float>& label_array);

    void setNewClearPreviousColourLabels(std::vector<CVD::Rgb<CVD::byte> >newColours);

    void setNewClearPreviousColourLabels(std::string fileName)
    {
        for(int l = 0; l < 255; l++)
        {
            colours[l] = -1;
        }

        ifstream ifile(fileName.c_str());

        char readlinedata[50];

        int label, red, green, blue;
        int count=0;

        while(1)
        {
            ifile.getline(readlinedata,50);

            if (ifile.eof())
                break;

            istringstream iss(readlinedata);

            iss >> label;
            iss >> red;
            iss >> green;
            iss >> blue;

            int label_val = (int)(red + green*256 + blue*256*256);

            colours[label] = label_val;

            count++;
        }

        ifile.close();

        no_labels = count;

        std::cout<<"New Labels!" << std::endl;

        for(int l =0; l < 255; l++)
        {
            if ( colours[l] != -1 )
            {
                CVD::Rgb<CVD::byte>pixel_colour;

                int label_val       = colours[l];

                int label_val_hat   = (label_val)%(256*256);

                pixel_colour.blue   = (label_val - label_val_hat)/(256*256);
                pixel_colour.green  = (label_val_hat - label_val_hat%256)/256;
                pixel_colour.red    = (label_val_hat%256);

                std::cout<<"[l = " << l<<"] ";

                std::cout<<"red = " << (int)pixel_colour.red
                         <<", green =" << (int)pixel_colour.green
                         <<", blue = "<<(int)pixel_colour.blue<<std::endl;
            }
        }
    }


    void writeLabelsToFile(std::string fileName)
    {
        ofstream ofile(fileName.c_str());

        for(int l = 0; l < no_labels; l++)
        {
            CVD::Rgb<CVD::byte>pixel_colour;

            int label_val       = colours[l];

            int label_val_hat   = (label_val)%(256*256);

            pixel_colour.blue   = (label_val - label_val_hat)/(256*256);
            pixel_colour.green  = (label_val_hat - label_val_hat%256)/256;
            pixel_colour.red    = (label_val_hat%256);

            ofile <<l<<" "<< (int)pixel_colour.red <<" "<<(int)pixel_colour.green<<" "<<(int)pixel_colour.blue<<std::endl;
        }

        ofile.close();
    }


    /// Get the standard parmeters
    int getHeight(){ return img_height; }
    int getWidth() { return img_width ; }
    int* getColours(){ return colours; }



    void loadDepthImageVaFRIC(const unsigned int ref_img_no,
                              float* depth_array,
                              unsigned int hstridef1,
                              unsigned int width,
                              unsigned int height,
                              const float n_plane,
                              const float f_plane);

    void loadDepthImage(std::string& fileName, float* depth_array, unsigned int hstridef1,
                        unsigned int width, unsigned int height, const float n_plane, const float f_plane);
    void loadDepthImageTUM(std::string& fileName, float* depth_array, unsigned int hstridef1,
                        unsigned int width, unsigned int height, const float n_plane, const float f_plane);

    /// Wrapper for reading the depth file
    void readDepthFile(int ref_img_no, int which_blur_sample, std::vector<float>& depth_array);

    /// Wrapper for getting the 3D positions
    void get3Dpositions(int ref_img_no, int which_blur_sample, float4* points3D);
    void get3DpositionsNormalised(int ref_img_no, int which_blur_sample, float4* points3D);
    void get3DpositionsFromDepthFile(std::string& fileName, float4* points3D,
                                     float2 fl, float2 pp,
                                     int width, int height,
                                     float near_plane, float far_plane);

    /// Wrapper for getting the normals
    void getNormalsFromDepth(int ref_img_no, int which_blur_sample, std::vector<float>& normal_array);
    void getNormals_wrefframe_FromDepth(int ref_img_no, int which_blur_sample, std::vector<float>& normal_array);

    void convertNormals2Image(int ref_img_no, int which_blur_sample,
                              std::vector<float>& normal_array,
                              std::string& normal_file_name_in_png)
    {
        CVD::Image< CVD::Rgb<CVD::byte> >normalImage(CVD::ImageRef(img_width,img_height));

        int i = 0;

        for(int yy = 0; yy < img_height ; yy++ )
        {
            for(int xx = 0; xx < img_width; xx++ )
            {
                normalImage[CVD::ImageRef(xx,yy)] = CVD::Rgb<CVD::byte>(
                            (unsigned char)(normal_array[i+0]*127.f+127.f),
                            (unsigned char)(normal_array[i+1]*127.f+127.f),
                            (unsigned char)(normal_array[i+2]*127.f+127.f)
                            );
                i = i + 3;
            }
        }

        CVD::img_save(normalImage,normal_file_name_in_png.c_str());
    }

    void get3DpositionsFromDepthFileTUM(std::string& fileName,
                                        float4* points3D,
                                        float2 fl,
                                        float2 pp,
                                        int width,
                                        int height,
                                        float near_plane,
                                        float far_plane);

    void getRGBImage(int ref_img_no,
                      int which_blur_sample,
                      float4* h_image,
                      const unsigned int hstridef4,
                      const unsigned int width,
                      const unsigned int height);

    /// Get the number of relevant files
    int getNumberofPoseFiles() { return  txtfilecount ; }
    int getNumberofImageFiles(){ return pngfilecount  ; }
    int getNumberofDepthFiles(){ return depthfilecount; }

    std::string getImageFullPath(int ref_img_no, int which_blur_sample)
    {
        char png_file_name[360];
        sprintf(png_file_name,"%s/scene_%02d_%04d.png",filebasename.c_str(),
                which_blur_sample,ref_img_no);

        return std::string(png_file_name);
    }

    /// Get the png file
    template <class T>
    CVD::Image<T> getPNGImage(int ref_img_no, int which_blur_sample)
    {
        char png_file_name[360];
        sprintf(png_file_name,"%s/scene_%02d_%04d.png",filebasename.c_str(),
                which_blur_sample,ref_img_no);

        CVD::Image<T> img;
        CVD::img_load(img,png_file_name);

        return img;
    }

    /// Get the labelled colours
    void getColourArrayofLabels(std::vector<int>&_colours)
    {
        if(_colours.size())
            _colours.resize(255);
        else
            _colours = std::vector<int>(255);


        for(int i = 0; i < 255 ;i++)
        {
            _colours.at(i) = colours[i];
        }
    }

    /// Convert Depth to TUM format
    void convertPOV2TUMformat(float* pov_format, float* tum_format, int scale_factor);
    void convertPOV2TUMformat(float* pov_format, u_int16_t* tum_format, int scale_factor);

    /// Convert Depth to Normalised Float PNG
    void convertDepth2NormalisedFloat(float* depth_arrayIn, float* depth_arrayOut, int scale_factor);

    /// Convert Depth to Normalised Float [min,max] -> [0,1]
    void convertDepth2NormalisedFloat(float *depth_arrayIn,
                                              float *depth_arrayOut,
                                              float max_depth, float min_depth);

    /// Convert Depth to Normal
    void convertDepth2NormalImage(int ref_img_no, int which_blur_sample, string imgName);


    void addHoles(float* depth_image,
                  const unsigned int hstridef1,
                  const unsigned int height,
                  const unsigned int n_holes);

    /// Add Noise to the Depth Value
    /// \sigma(z,\theta) = z_{1} + z_{2}(z - z_{3})^2 + (z_{3}/\sqrt{z})*(\theta/(pi/2-theta))^2;
    void addDepthNoise(std::vector<float> &depth_arrayIn, std::vector<float> &depth_arrayOut,
                       float z1, float z2, float z3, int ref_image_no,
                       int which_blur );

private:

    /// Directory variables
    string filebasename;
    int txtfilecount;
    int pngfilecount;
    int depthfilecount;

    /// Camera Intrincs
    int img_width, img_height;
    float u0, v0, focal_x, focal_y;

    ///Labelling Related
    int colours[255];
    int no_labels;
};

}

#endif
